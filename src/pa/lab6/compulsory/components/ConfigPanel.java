package pa.lab6.compulsory.components;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ConfigPanel extends JPanel {
    final MainFrame frame;
    JLabel sidesLabel;
    static JSpinner sidesField;
    JComboBox colorCombo;
    String[] colors = {"Random", "Black"};
    Color color = null;
    int size;
    JLabel sizeInput;
    JTextField sizeInputField;
    JLabel colorLabel;


    public ConfigPanel(MainFrame frame) {
        this.frame = frame;
        init();
    }

     /**
     * The init method initializes the canvas where we will draw the shapes
     * I created three fields: one for choosing the size of the shape,
     * one for choosing how many sides the shape will have and another one
     * for choosing the color of the shape (either random color or black)
     * I overridden the actionPerformed() method in order to take the input from the user
     * (you just have to write the size of the shape and press enter, then click on the canvas)
     *
     * Furthermore, I overridden it for the color field as well, so I can choose between black and random
     * by clicking on the field.
     */

    private void init() {

        this.setBorder(BorderFactory.createDashedBorder(Color.black));
        this.setLayout(new FlowLayout(FlowLayout.CENTER, 20,20));

        sizeInput = new JLabel("Size: ");
        sizeInput.setFont(new Font("Times New Roman",Font.BOLD, 17));

        sizeInputField = new JTextField("Choose your size :D", 20);
        sizeInputField.setFont(new Font("Times New Roman", Font.BOLD, 17));
        sizeInputField.setBorder(BorderFactory.createDashedBorder(Color.red));

        sizeInputField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                size = Integer.parseInt(sizeInputField.getText());
            }
        });

        sidesLabel = new JLabel("Number of sides:");
        sidesLabel.setFont(new Font("Times New Roman", Font.BOLD, 17));

        sidesField = new JSpinner(new SpinnerNumberModel(1,1,20,1));
        sidesField.setValue(6);
        sidesField.setFont(new Font("Times New Roman", Font.BOLD, 17));

        colorLabel = new JLabel("Color: ");
        colorLabel.setFont(new Font("Times New Roman", Font.BOLD, 17));
        colorCombo = new JComboBox<>(colors);

        ActionListener comboActionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object[] selectedColor = colorCombo.getSelectedObjects();
                if(selectedColor[0].equals("Black")){
                    color = Color.black;
                }
                else{
                    color = null;
                }
            }
        };

        colorCombo.addActionListener(comboActionListener);


        add(sizeInput);
        add(sizeInputField);
        add(sidesLabel);
        add(sidesField);
        add(colorLabel);
        add(colorCombo);

    }
}

