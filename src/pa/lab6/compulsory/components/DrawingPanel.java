package pa.lab6.compulsory.components;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.Random;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.awt.event.MouseAdapter;

public class DrawingPanel extends JPanel {
    final MainFrame frame;
    int W = 1000, H = 700;
    BufferedImage image;
    Graphics2D graphics;

     /**
     * Here I create the screen where I intend to draw some cool shapes
     * I use an object of the subclass BufferedImage and one of Graphics2D
     * I call the createGraphics(), it creates a Graphics2D, which can be used to draw into this BufferedImage,
     * I choose the white color for the canvas.
     */

    void createOffScreenImage() {
        image = new BufferedImage(W, H, BufferedImage.TYPE_INT_ARGB);
        graphics = image.createGraphics();
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, W, H);
    }

     /**
     * Here, when I click on the canvas, a shape will appear on the screen
     */

    private void init() {
        setPreferredSize(new Dimension(W, H));
        setBorder(BorderFactory.createEtchedBorder());
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                drawShape(e.getX(), e.getY());
                repaint();
            }
        });
    }

    public DrawingPanel(MainFrame frame) {
        this.frame = frame;
        createOffScreenImage();
        init();
    }

    private void drawShape(int x, int y) {
        int radius = frame.configPanel.size;
        int sides = (Integer) ConfigPanel.sidesField.getValue();
        Color color = frame.configPanel.color;
        Random random = new Random();


        if(color == null) {
            color = new Color(random.nextInt(128) + 128, random.nextInt(128) + 128,
                    random.nextInt(128) + 128);
        }

        graphics.setColor(color);
        graphics.fill(new RegularPolygon(x, y, radius, sides));
    }

    @Override
    public void update(Graphics g) { }

    @Override
    protected void paintComponent(Graphics g) {
        g.drawImage(image, 0, 0, this);
    }
}
