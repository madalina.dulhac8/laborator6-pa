package pa.lab6.compulsory.main;

import pa.lab6.compulsory.components.MainFrame;

public class Main {
    public static void main(String[] args) {
        new MainFrame().setVisible(true);
    }
}
